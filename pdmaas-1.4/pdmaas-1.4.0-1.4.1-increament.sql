ALTER TABLE MAAS_USER_LOG
ADD COLUMN `UPDATE_CONTENT` JSON COMMENT '修改内容;' AFTER OPERATON_TYPE;

ALTER TABLE CORE_PROJECT_VERSION
ADD COLUMN `VERSION_DDL` TEXT COMMENT '当前版本较前一版DDL;' AFTER DIFF_CONTENT;

/* --------------- 修改表 --------------- */
-- 修改表：CORE_PROJECT_VERSION[项目版本]
-- 修改字段：version_comment
ALTER TABLE CORE_PROJECT_VERSION  MODIFY COLUMN `VERSION_COMMENT` TEXT COMMENT '版本说明';

-- 此条sql如果执行错误，请查看readme.md文件
alter table core_entity  
 add constraint core_entity_pk  
 unique (PROJECT_ID, ENTITY_ID);