-- 此文档用于历史版本导致的数据重复存储问题，sql较为简单，在delete对应数据前，建议可以对数据先进行备份 ---
# ----------------------- dict字典 ----------------------------#
# core_dict和core_dict_item字典去重操作。
delete from core_dict where SERIAL_ID in (
    select SERIAL_ID from (
        -- 以下为查询后备份sql，后续sql类似 -- 
        select * from core_dict
        where DICT_ID in (
            select DICT_ID
            from core_dict
            group by PROJECT_ID,DICT_ID having count(*) >=2)
        and SERIAL_ID not in (
            select min(SERIAL_ID)
            from core_dict
            group by PROJECT_ID,DICT_ID
            having count(*)>=2
        )
        -- 以上为查询后备份sql --
    )t
) ;

delete from core_dict_item where SERIAL_ID in (
    select SERIAL_ID from (
        select * from core_dict_item
        where DICT_ITEM_ID in (
            select DICT_ITEM_ID
            from core_dict_item
            group by PROJECT_ID,DICT_ID,DICT_ITEM_ID having count(*) >=2)
        and SERIAL_ID not in (
            select min(SERIAL_ID)
            from core_dict_item
            group by PROJECT_ID,DICT_ID,DICT_ITEM_ID having count(*) >=2
        )
    ) t
);
# 增加唯一索引
alter table core_dict
    add constraint core_dict_pk
        unique (PROJECT_ID, DICT_ID);

alter table core_dict_item
    add constraint core_dict_item_pk
        unique (PROJECT_ID,DICT_ID,DICT_ITEM_ID);

# ----------------------- diagram关系图 ---------------------------- #

delete from core_diagram where SERIAL_ID in (
    select SERIAL_ID from (
        select * from core_diagram
        where DIAGRAM_ID in (
            select DIAGRAM_ID
            from core_diagram
            group by PROJECT_ID,DIAGRAM_ID having count(*) >=2 )
        and SERIAL_ID not in (
            select min(SERIAL_ID)
            from core_diagram
            group by PROJECT_ID,DIAGRAM_ID having count(*) >=2
        )
    ) t
);

alter table core_diagram
    add constraint core_diagram_pk
        unique (PROJECT_ID,DIAGRAM_ID);

# ----------------------- entity 实体与视图 ---------------------------- #

#删除重复的索引
DELETE from core_entity_index WHERE SERIAL_ID in (
SELECT SERIAL_ID from (
    SELECT * from core_entity_index WHERE INDEX_ID in (
         SELECT INDEX_ID 
         from core_entity_index 
         GROUP BY PROJECT_ID,ENTITY_SERIAL_ID,INDEX_ID HAVING COUNT(*)>=2) 
    and SERIAL_ID not in (
         SELECT MIN(SERIAL_ID) 
         from core_entity_index 
         GROUP BY PROJECT_ID,ENTITY_SERIAL_ID,INDEX_ID 
         HAVING COUNT(*)>=2) 
    ) t
)

#删除重复的索引字段
DELETE from core_entity_index_field WHERE SERIAL_ID in (
SELECT SERIAL_ID FROM (
     SELECT * from core_entity_index_field WHERE INDEX_FIELD_ID in (
         SELECT INDEX_FIELD_ID 
         from core_entity_index_field 
         GROUP BY PROJECT_ID,INDEX_SERIAL_ID,INDEX_FIELD_ID 
         HAVING COUNT(*)>=2) 
    and SERIAL_ID not in (
        SELECT MIN(SERIAL_ID) 
        from core_entity_index_field 
        GROUP BY PROJECT_ID,INDEX_SERIAL_ID,INDEX_FIELD_ID 
        HAVING COUNT(*)>=2) 
    ORDER BY INDEX_FIELD_ID) t
)

delete from core_entity where SERIAL_ID in (
    select SERIAL_ID from (
        select * from core_entity
        where ENTITY_ID in (
            select ENTITY_ID 
            from core_entity
            group by PROJECT_ID,ENTITY_ID,ENTITY_TYPE having count(*)>=2)
        and SERIAL_ID not in (
            select min(SERIAL_ID)
            from core_entity
            group by PROJECT_ID,ENTITY_TYPE,ENTITY_TYPE having count(*)>=2
        )
    ) t
);

delete from core_entity_field where SERIAL_ID in (
    select SERIAL_ID from (
        select * from core_entity_field
        where FIELD_ID in (
            select FIELD_ID
            from core_entity_field
            group by PROJECT_ID,FIELD_ID,ENTITY_SERIAL_ID having count(*)>=2)
        and SERIAL_ID not in (
            select min(SERIAL_ID)
            from core_entity_field
            group by PROJECT_ID,FIELD_ID,ENTITY_SERIAL_ID having count(*)>=2)
    ) t
);

alter table core_entity  
    add constraint core_entity_pk  
        unique (PROJECT_ID, ENTITY_ID);

alter table core_entity_field 
    add constraint core_entity_field_pk
        unique (PROJECT_ID,FIELD_ID,ENTITY_SERIAL_ID)

alter table core_entity_index
    add constraint core_entity_index_pk
        unique (PROJECT_ID,INDEX_ID,ENTITY_SERIAL_ID);

-- alter table core_entity_index_field
--     add constraint core_entity_index_field_pk
--         unique (PROJECT_ID,INDEX_FIELD_ID,INDEX_SERIAL_ID);

# ----------------------- domain 数据域 ---------------------------- #

delete from core_domain where SERIAL_ID in (
    select SERIAL_ID from (
        select * from core_domain
        where DOMAIN_ID in (
            select DOMAIN_ID
            from core_domain
            group by PROJECT_ID,DOMAIN_ID having count(*)>=2)
        and SERIAL_ID not in (
            select min(SERIAL_ID)
            from core_domain
            group by PROJECT_ID,DOMAIN_ID having count(*)>=2)
    ) t
);

alter table core_domain
    add constraint core_domain_pk
        unique (PROJECT_ID, DOMAIN_ID);