alter table core_project
    add USE_TEMP_ID varchar(36) null;

alter table core_data_type
    add SORT_NO int null;

alter table core_domain
    add SORT_NO int null;


-- 针对日志文件，如果确保之前的保存日志无效，建议修改对应的表结构，并清除原有日志信息 --
-- 说明：之前针对协同用户信息，我们期望直接存储数据库，然而由于数据量大小差异过大，表明此设计的不合理，因此在1.5.2对该问题做出优化 -- 
drop table if exists maas_user_log;
create table if not exists maas_user_log
(
    REVISION       varchar(36) null comment '乐观锁',
    CREATED_BY     varchar(36) null comment '创建人',
    CREATED_TIME   varchar(19) null comment '创建时间',
    UPDATED_BY     varchar(36) null comment '更新人',
    UPDATED_TIME   varchar(19) null comment '更新时间',
    SERIAL_ID      varchar(32) not null comment '序列号;',
    USER_ID        varchar(32) null comment '用户ID;',
    TENANT_ID      varchar(32) null comment '企业ID;用户查看那些企业是活跃的',
    PROJECT_ID     varchar(32) null comment '项目ID;用于查看那些项目是活跃的',
    OPERATON_TYPE  varchar(32) null comment '操作类型;',
    ALL_CONTENT    json        null,
    UPDATE_CONTENT varchar(90) null,
    PRIMARY KEY (SERIAL_ID)
)
     ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户日志' ROW_FORMAT = DYNAMIC;
