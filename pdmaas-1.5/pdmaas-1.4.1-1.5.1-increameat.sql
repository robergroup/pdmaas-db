/* --------------- 修改表 --------------- */
-- 修改表：CORE_PROJECT_VERSION[项目版本]
-- 修改字段：
ALTER TABLE CORE_PROJECT_VERSION  MODIFY COLUMN `VERSION_COMMENT` VARCHAR(9000) COMMENT '版本说明';
ALTER TABLE CORE_PROJECT_VERSION  MODIFY COLUMN `VERSION_DDL` MEDIUMTEXT COMMENT '当前版本较前一版DDL';
/* --------------- 修改表 --------------- */
-- 修改表：MAAS_USER_LOG[用户日志]
-- 修改字段：
ALTER TABLE MAAS_USER_LOG  MODIFY COLUMN `UPDATE_CONTENT` MEDIUMTEXT COMMENT '修改内容';
-- 添加字段：
ALTER TABLE MAAS_USER_LOG ADD COLUMN `ALL_CONTENT` JSON COMMENT '全量内容;' AFTER UPDATE_CONTENT;

alter table core_dict_item change SORT_ SORT varchar(255) null comment '排序顺序号';
alter table core_dict_item change ENABLED_ ENABLED varchar(1) null comment '是否启用';