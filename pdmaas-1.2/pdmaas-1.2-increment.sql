
/* --------------- 修改表 --------------- */
-- 修改表：MAAS_USER[用户信息]
-- 添加字段：
ALTER TABLE MAAS_USER
ADD COLUMN LAST_LOGIN VARCHAR(19) COMMENT '上次登录;' AFTER UPDATED_TIME;
/* --------------- 修改表 --------------- */
-- 修改表：CORE_TOPIC[分类主题]
-- 添加字段：
ALTER TABLE CORE_TOPIC
ADD COLUMN SORT_NO INT COMMENT '排序字段;' AFTER UPDATED_TIME;
/* --------------- 修改表 --------------- */
-- 修改表：CORE_ENTITY[实体/视图]
-- 修改字段：
ALTER TABLE CORE_ENTITY  MODIFY COLUMN DEF_NAME VARCHAR(255) COMMENT '实体名称';
/* --------------- 修改表 --------------- */
-- 修改表：CORE_DICT[数据字典]
-- 添加字段：
ALTER TABLE CORE_DICT
ADD COLUMN SORT_NO INT COMMENT '排序字段;' AFTER UPDATED_TIME;
/* --------------- 修改表 --------------- */
-- 修改表：CORE_DIAGRAM[关系图]
-- 添加字段：
ALTER TABLE CORE_DIAGRAM
ADD COLUMN SORT_NO INT COMMENT '排序字段;' AFTER UPDATED_TIME;