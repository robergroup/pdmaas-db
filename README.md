##  创建MySQL数据库，并且初始化基础数据

### 1 安装MySQL数据库

MySQL数据库，可以直接安装，也可以通过docker镜像安装（如已安装，则跳过这一步）

拉取镜像：

```
docker pull mysql/mysql-server:latest #拉取MySQL镜像，你也可以根据需要，指定版本
```

运行容器

```
docker run -itd --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 -d mysql/mysql-server --lower_case_table_names=1
```

参数说明：

-p 3306:3306 ：映射容器服务的 3306 端口到宿主机的 3306 端口，外部主机可以直接通过 宿主机ip:3306 访问到 MySQL 的服务。
MYSQL_ROOT_PASSWORD=123456：设置 MySQL 服务 root 用户的密码。
--lower_case_table_names=1：数据库表名忽略大小写

### 2 创建空数据库

如果是docker安装的MySQL，则通过命令：```docker exec -it mysql bash```进入容器。
进入MySQL命令行，执行命令：

```
mysql -u root -p 123456

#授权
CREATE USER 'root'@'%' IDENTIFIED BY 'root';
GRANT ALL ON *.* TO 'root'@'%';

#刷新权限
flush privileges;

#修改root用户密码
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '123456';
#刷新权限
flush privileges;
```



### 3. 导入初始数据【增量脚本请查看对应目录下的readme.md】

**1.下载初始数据**

下载工程目录下对应于您pdmaas版本的pdmaas-${版本号}.sql

**2.导入数据文件**

```
CREATE DATABASE pdmaas DEFAULT CHARACTER SET utf8 ;
source ${你SQL文件下载目录}/pdmaas-${版本号}.sql
```

## 