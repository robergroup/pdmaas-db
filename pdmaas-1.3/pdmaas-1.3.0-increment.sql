DROP TABLE IF EXISTS CORE_COMPARISON_DATA;
CREATE TABLE CORE_COMPARISON_DATA(
    `SERIAL_ID` VARCHAR(36) NOT NULL   COMMENT '流水号' ,
    `PROJECT_ID` VARCHAR(36)    COMMENT '项目ID' ,
    `NAME` VARCHAR(90)    COMMENT '名称' ,
    `INTRO` VARCHAR(900)    COMMENT '项目说明' ,
    `DATA_SOURCE_TYPE` VARCHAR(32)    COMMENT '数据源类型' ,
    `SOURCE_URL` VARCHAR(255)    COMMENT '数据提取URL' ,
    `JSON_TEXT` JSON    COMMENT 'json内容' ,
    `REVISION` VARCHAR(36)    COMMENT '乐观锁' ,
    `CREATED_BY` VARCHAR(36)    COMMENT '创建人' ,
    `CREATED_TIME` VARCHAR(19)    COMMENT '创建时间' ,
    `UPDATED_BY` VARCHAR(36)    COMMENT '更新人' ,
    `UPDATED_TIME` VARCHAR(19)    COMMENT '更新时间' ,
    PRIMARY KEY (SERIAL_ID)
)  COMMENT = '项目比对数据源';
DROP TABLE IF EXISTS MAAS_MAINTENANCE_LOG;
CREATE TABLE MAAS_MAINTENANCE_LOG(
    `LOG_ID` VARCHAR(255) NOT NULL   COMMENT '日志id' ,
    `MAINTENANCE_STATUS` VARCHAR(255)    COMMENT '维护状态' ,
    `NOTICE_TITLE` VARCHAR(90)    COMMENT '维护公告' ,
    `NOTICE_TEXT` VARCHAR(900)    COMMENT '维护内容' ,
    `VERSION` VARCHAR(255)    COMMENT '版本' ,
    `REVISION` VARCHAR(36)    COMMENT '乐观锁' ,
    `CREATED_BY` VARCHAR(36)    COMMENT '创建人' ,
    `CREATED_TIME` VARCHAR(19)    COMMENT '创建时间' ,
    `UPDATED_BY` VARCHAR(36)    COMMENT '更新人' ,
    `UPDATED_TIME` VARCHAR(19)    COMMENT '更新时间' ,
    PRIMARY KEY (LOG_ID)
)  COMMENT = '版本维护日志';

/* --------------- 修改表 --------------- */
-- 修改表：core_diagram[关系图]
-- 修改字段：
ALTER TABLE core_diagram  MODIFY COLUMN `SORT_NO` INT COMMENT '排序号';
/* --------------- 修改表 --------------- */
-- 修改表：core_dict[数据字典]
-- 修改字段：
ALTER TABLE core_dict  MODIFY COLUMN `SORT_NO` INT COMMENT '排序号';
/* --------------- 修改表 --------------- */
-- 修改表：core_entity[实体/视图]
-- 修改字段：
ALTER TABLE core_entity  MODIFY COLUMN `DEF_NAME` VARCHAR(900) COMMENT '实体名称';
/* --------------- 修改表 --------------- */
-- 修改表：core_entity_field[实体字段]
-- 修改字段：
ALTER TABLE core_entity_field  MODIFY COLUMN `DEF_NAME` VARCHAR(900) COMMENT '字段名称';

/* --------------- 修改表 --------------- */
-- 修改表：core_project_profile[项目设置]
-- 添加字段：
ALTER TABLE core_project_profile
ADD COLUMN `META_DATA` JSON COMMENT '比对数据源;' AFTER RECENT_COLORS;
/* --------------- 修改表 --------------- */
-- 修改表：core_topic[分类主题]
-- 修改字段：
ALTER TABLE core_topic  MODIFY COLUMN `SORT_NO` INT COMMENT '排序号';
/* --------------- 修改表 --------------- */
-- 修改表：maas_order[订单]
-- 添加字段：
ALTER TABLE maas_order
ADD COLUMN `ORDER_MONTHS` INT COMMENT '订单时间;' AFTER UPDATED_TIME;
/* --------------- 修改表 --------------- */
-- 修改表：maas_price_system[定价系统]
-- 添加字段：
ALTER TABLE maas_price_system
ADD COLUMN `MONTHS` INT COMMENT '服务时限;' AFTER UPDATED_TIME;